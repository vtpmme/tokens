#!/bin/bash


#Deploying, Issueing and Distributing Tokens
$issuer="default"
$supply=10000000.0000
$symbol="NULL"



#1. Compile the token
eosio-cpp -I include -o eosio.token $(pwd)/eosio.contracts/eosio.token/src/eosio.token.cpp --abigen


#2. Create token account
cleos create account eosio eosio.token EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV

#3. Deploy the token contract
eosio-cpp set contract eosio.token $(pwd)/eosio.contracts/contracts/eosio.token --abi eosio.token.abi -p eosio.token@active


cleos push action eosio.token create '['+"\""+$(issuer) + ",\"" + $(supply) +"]' -p eosio.token@active



